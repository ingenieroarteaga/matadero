import { Component} from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth/auth.service';
import { TokenService } from '../services/token/token.service';


@Component({
  selector: 'ngx-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [AuthService, TokenService],
})

export class LoginComponent {

  username: string;
  password: string;

  constructor(private router: Router, private authService: AuthService,
              private token: TokenService) {

                const el = document.getElementById('nb-global-spinner');
                  if (el) {
                    el.style['display'] = 'none';
                }
  }

  ingresar(): void {
    this.authService.attemptAuth(this.username, this.password).subscribe(
      data => {
        this.token.saveToken(data.token);
        console.log('apreto el boton');
        this.router.navigate(['pages/clasificadores/paises']);
      },
    );
  }

}
