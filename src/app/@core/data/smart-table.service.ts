import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';
// CLASIFICADORES
import {Pais} from '../../../app/pages/clasificadores/paises/pais';
import {Provincia} from '../../../app/pages/clasificadores/provincias/provincia';
import {Localidad} from '../../../app/pages/clasificadores/localidades/localidad';
import {Distrito} from '../../../app/pages/clasificadores/distritos/distrito';
import {TipoDomicilio} from '../../../app/pages/clasificadores/tipos-domicilio/tipo-domicilio';
import {TipoDocumento} from '../../../app/pages/clasificadores/tipos-documento/tipo-documento';
import {TipoAnimal} from '../../../app/pages/clasificadores/tipos-animales/tipo-animal';
import {SubTipoAnimal} from '../../../app/pages/clasificadores/subtipos-animales/subtipo-animal';
import {TipoCamara} from '../../../app/pages/clasificadores/tipos-camaras/tipo-camara';
// HERRRAMIENTAS
// PERSONAS
import {Productor} from '../../../app/pages/personas/productores/productor';
import {Matarife} from '../../../app/pages/personas/matarifes/matarife';
import {Transportista} from '../../../app/pages/personas/transportistas/transportista';
import {Administrativo} from '../../../app/pages/personas/administrativos/administrativo';
import {Operario} from '../../../app/pages/personas/operarios/operario';
import {Corralero} from '../../../app/pages/personas/corraleros/corralero';

import { Global } from '../../../app/global';

@Injectable()
export class SmartTableService {

  paises: any = [];
  source: any = [];
  path: string = Global.urlBase;
  private httpHeaders = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(public http: HttpClient) {

   }

  // Servicios para PAIS

  getPaises() {
    return this.http.get<Pais[]>(this.path + '/paises');
  }

  createPais(pais: Pais): Observable<Pais> {
    return this.http.post<Pais>(this.path + '/pais', pais, { headers: this.httpHeaders });
  }

  updatePais(pais: Pais): Observable<Pais> {
    return this.http.put<Pais>(this.path + '/pais', pais, { headers: this.httpHeaders });
  }

  deletePais(id: number): Observable<Pais> {
    return this.http.delete<Pais>(`${this.path + '/pais'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para PROVINCIA

  getProvincias() {
    return this.http.get<Provincia[]>(this.path + '/provincias');
  }

  createProvincia(provincia: Provincia): Observable<Provincia> {
    return this.http.post<Provincia>(this.path + '/provincia', provincia, { headers: this.httpHeaders });
  }

  updateProvincia(provincia: Provincia): Observable<Provincia> {
    return this.http.put<Provincia>(this.path + '/provincia', provincia, { headers: this.httpHeaders });
  }

  deleteProvincia(id: number): Observable<Provincia> {
    return this.http.delete<Provincia>(`${this.path + '/provincia'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para LOCALIDADES

  getLocalidades() {
    return this.http.get<Localidad[]>(this.path + '/localidades');
  }

  createLocalidad(localidad: Localidad): Observable<Localidad> {
    return this.http.post<Localidad>(this.path + '/localidad', localidad, { headers: this.httpHeaders });
  }

  updateLocalidad(localidad: Localidad): Observable<Localidad> {
    return this.http.put<Localidad>(this.path + '/localidad', localidad, { headers: this.httpHeaders });
  }

  deleteLocalidad(id: number): Observable<Localidad> {
    return this.http.delete<Localidad>(`${this.path + '/localidad'}/${id}`, { headers: this.httpHeaders });
  }
  // Servicios para Distritos

  getDistritos() {
    return this.http.get<Distrito[]>(this.path + '/distritos');
  }

  createDistrito(localidad: Distrito): Observable<Distrito> {
    return this.http.post<Distrito>(this.path + '/distrito', localidad, { headers: this.httpHeaders });
  }

  updateDistrito(localidad: Distrito): Observable<Distrito> {
    return this.http.put<Distrito>(this.path + '/distrito', localidad, { headers: this.httpHeaders });
  }

  deleteDistrito(id: number): Observable<Distrito> {
    return this.http.delete<Distrito>(`${this.path + '/distrito'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para TIPO DE DOMICILIO

  getTipoDomicilios() {
    return this.http.get<TipoDomicilio[]>(this.path + '/tipos-domicilios');
  }

  createTipoDomicilio(tipoDomicilio: TipoDomicilio): Observable<TipoDomicilio> {
    return this.http.post<TipoDomicilio>(this.path + '/tipo-domicilio', tipoDomicilio, { headers: this.httpHeaders });
  }

  updateTipoDomicilio(tipoDomicilio: TipoDomicilio): Observable<TipoDomicilio> {
    return this.http.put<TipoDomicilio>(this.path + '/tipo-domicilio', tipoDomicilio, { headers: this.httpHeaders });
  }

  deleteTipoDomicilio(id: number): Observable<TipoDomicilio> {
    return this.http.delete<TipoDomicilio>(`${this.path + '/tipo-domicilio'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para TIPO DOCUMENTO

  getTipoDocumentos() {
    return this.http.get<TipoDocumento[]>(this.path + '/tipos-documentos');
  }

  createTipoDocumento(localidad: TipoDocumento): Observable<TipoDocumento> {
    return this.http.post<TipoDocumento>(this.path + '/tipo-documento', localidad, { headers: this.httpHeaders });
  }

  updateTipoDocumento(localidad: TipoDocumento): Observable<TipoDocumento> {
    return this.http.put<TipoDocumento>(this.path + '/tipo-documento', localidad, { headers: this.httpHeaders });
  }

  deleteTipoDocumento(id: number): Observable<TipoDocumento> {
    return this.http.delete<TipoDocumento>(`${this.path + '/tipo-documento'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para TIPO ANIMAL

  getTiposAnimales() {
    return this.http.get<TipoAnimal[]>(this.path + '/tipos-animales');
  }

  createTipoAnimal(tipoAnimal: TipoAnimal): Observable<TipoAnimal> {
    return this.http.post<TipoAnimal>(this.path + '/tipo-animal', tipoAnimal, { headers: this.httpHeaders });
  }

  updateTipoAnimal(tipoAnimal: TipoAnimal): Observable<TipoAnimal> {
    return this.http.put<TipoAnimal>(this.path + '/tipo-animal', tipoAnimal, { headers: this.httpHeaders });
  }

  deleteTipoAnimal(id: number): Observable<TipoAnimal> {
    return this.http.delete<TipoAnimal>(`${this.path + '/tipo-animal'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para SUB TIPO ANIMAL

  getSubTiposAnimales() {
    return this.http.get<SubTipoAnimal[]>(this.path + '/subtipos-animales');
  }

  createSubTipoAnimal(subTipoAnimal: SubTipoAnimal): Observable<SubTipoAnimal> {
    return this.http.post<SubTipoAnimal>(this.path + '/subtipo-animal', subTipoAnimal, { headers: this.httpHeaders });
  }

  updateSubTipoAnimal(subTipoAnimal: SubTipoAnimal): Observable<SubTipoAnimal> {
    return this.http.put<SubTipoAnimal>(this.path + '/subtipo-animal', subTipoAnimal, { headers: this.httpHeaders });
  }

  deleteSubTipoAnimal(id: number): Observable<SubTipoAnimal> {
    return this.http.delete<SubTipoAnimal>(`${this.path + '/subtipo-animal'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para TIPO CAMARAS

  getTiposCamaras() {
    return this.http.get<TipoCamara[]>(this.path + '/tipos-camaras');
  }

  createTipoCamara(localidad: TipoCamara): Observable<TipoCamara> {
    return this.http.post<TipoCamara>(this.path + '/tipo-camara', localidad, { headers: this.httpHeaders });
  }

  updateTipoCamara(localidad: TipoCamara): Observable<TipoCamara> {
    return this.http.put<TipoCamara>(this.path + '/tipo-camara', localidad, { headers: this.httpHeaders });
  }

  deleteTipoCamara(id: number): Observable<TipoCamara> {
    return this.http.delete<TipoCamara>(`${this.path + '/tipo-camara'}/${id}`, { headers: this.httpHeaders });
  }


  // PERSONAS

  // Servicios para PRODUCTORES

  getProductores() {
    return this.http.get<Productor[]>(this.path + '/productores');
  }

  createProductor(productor: Productor): Observable<Productor> {
    return this.http.post<Productor>(this.path + '/productor', productor, { headers: this.httpHeaders });
  }

  updateProductor(productor: Productor): Observable<Productor> {
    return this.http.put<Productor>(this.path + '/productor', productor, { headers: this.httpHeaders });
  }

  deleteProductor(id: number): Observable<Productor> {
    return this.http.delete<Productor>(`${this.path + '/productor'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para MATARIFES

  getMatarifes() {
    return this.http.get<Matarife[]>(this.path + '/clientes');
  }

  createMatarife(matarife: Matarife): Observable<Matarife> {
    return this.http.post<Matarife>(this.path + '/cliente', matarife, { headers: this.httpHeaders });
  }

  updateMatarife(matarife: Matarife): Observable<Matarife> {
    return this.http.put<Matarife>(this.path + '/cliente', matarife, { headers: this.httpHeaders });
  }

  deleteMatarife(id: number): Observable<Matarife> {
    return this.http.delete<Matarife>(`${this.path + '/cliente'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para TRANPOSRTISTAS

  getTransportistas() {
    return this.http.get<Transportista[]>(this.path + '/choferes');
  }

  createTransportista(chofer: Transportista): Observable<Transportista> {
    return this.http.post<Transportista>(this.path + '/chofer', chofer, { headers: this.httpHeaders });
  }

  updateTransportista(chofer: Transportista): Observable<Transportista> {
    return this.http.put<Transportista>(this.path + '/chofer', chofer, { headers: this.httpHeaders });
  }

  deleteTransportista(id: number): Observable<Transportista> {
    return this.http.delete<Transportista>(`${this.path + '/chofer'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para Adminisrtativos

  getAdministrativo() {
    return this.http.get<Administrativo[]>(this.path + '/administrativos');
  }

  createAdministrativo(administrativo: Administrativo): Observable<Administrativo> {
    return this.http.post<Administrativo>(this.path + '/administrativo', administrativo, { headers: this.httpHeaders });
  }

  updateAdministrativo(administrativo: Administrativo): Observable<Administrativo> {
    return this.http.put<Administrativo>(this.path + '/administrativo', administrativo, { headers: this.httpHeaders });
  }

  deleteAdministrativo(id: number): Observable<Administrativo> {
    return this.http.delete<Administrativo>(`${this.path + '/administrativo'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para Operarios

  getOperario() {
    return this.http.get<Operario[]>(this.path + '/operarios');
  }

  createOperario(operario: Operario): Observable<Operario> {
    return this.http.post<Operario>(this.path + '/administrativo', operario, { headers: this.httpHeaders });
  }

  updateOperario(operario: Operario): Observable<Operario> {
    return this.http.put<Operario>(this.path + '/administrativo', operario, { headers: this.httpHeaders });
  }

  deleteOperario(id: number): Observable<Operario> {
    return this.http.delete<Operario>(`${this.path + '/administrativo'}/${id}`, { headers: this.httpHeaders });
  }

  // Servicios para Corraleros

  getCorraleros() {
    return this.http.get<Corralero[]>(this.path + '/corraleros');
  }

  createCorralero(corralero: Corralero): Observable<Corralero> {
    return this.http.post<Corralero>(this.path + '/corralero', corralero, { headers: this.httpHeaders });
  }

  updateCorralero(corralero: Corralero): Observable<Corralero> {
    return this.http.put<Corralero>(this.path + '/corralero', corralero, { headers: this.httpHeaders });
  }

  deleteCorralero(id: number): Observable<Corralero> {
    return this.http.delete<Operario>(`${this.path + '/corralero'}/${id}`, { headers: this.httpHeaders });
  }
}
