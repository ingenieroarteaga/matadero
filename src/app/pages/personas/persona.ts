// import { Cliente } from "../../personas/cliente/cliente";
// import { ContactoPersona } from "../contacto-persona";
// import { TipoDocumento } from "../../clasificadores/tipo-documento/tipo-documento";
// import { TipoPersona } from "../tipo-persona/tipo-persona";
// import { Proveedor } from "../../personas/proveedor/proveedor";
// import { DomicilioPersona } from "../domicilio-persona";

export class Persona {
    public id: number;
    public nombre: string;
    public apellido: string;
    public cuit: number;
    public dni: number;
    public cond_iva_id: number;
    public fechaNac: Date;
    public email: string;
    public telefono: string;
    public tipoDocumento: any;
    public tipoPersona: any;

}
