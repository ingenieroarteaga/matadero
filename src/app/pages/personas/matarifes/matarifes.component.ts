import { Component } from '@angular/core';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { Matarife } from './matarife';

@Component({
  selector: 'ngx-matarifes',
  templateUrl: './matarifes.component.html',
  styleUrls: ['./matarifes.component.scss'],
})
export class MatarifesComponent {

  // Add matarifes
  public matarife: Matarife = new Matarife();

  // Cargar matarifes en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      persona: {
        title: 'Nombre',
        type: 'string',
        valuePrepareFunction: (data) => {
                                   return data['nombre'];
                               },
      },
      apellido: {
        title: 'Apellido',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.persona.apellido;
          },
      },
      cuit: {
        title: 'Cuit',
        type: 'number',
        valuePrepareFunction: (cell, row) => {
            return row.persona.cuit;
          },
      },
      telefono: {
        title: 'Telefono',
        type: 'number',
        valuePrepareFunction: (cell, row) => {
            return row.persona.telefono;
          },
      },
      email: {
        title: 'E-mail',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.persona.email;
          },
      },
    },
  };

  constructor(public service: SmartTableService,
              ) {
                  // Obtengo los Matarifes
                this.service.getMatarifes().subscribe(
                  data => {
                      this.source = data;
                      console.log('Se hizo el traspaso de matarifes');
                    },
                  );

  }
  // Agregar matarife
  onCreateConfirm(event) {
    this.matarife = {'id': event.newData.id,
                  'persona': event.newData.persona,
                  'descripcion': event.newData.descripcion,
                  'fechaBaja': event.newData.fechaBaja,
                  };
      this.service.createMatarife(this.matarife).subscribe(datas => {
        event.confirm.resolve();
        console.log('se guardo un matarife', datas);
      });
  }

  // Borrar matarife
  onDeleteConfirm(event): void {
    if (window.confirm('Estás seguro que quieres borrar?')) {
      const idMatarife: number = event.newData.id;
      this.service.deleteMatarife(idMatarife).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el matarife', datas);
      });
    } else {
      event.confirm.reject();
    }
  }
  // Editar matarife
  onSaveConfirm(event) {
    this.matarife = {'id': event.newData.id,
                  'persona': event.newData.persona,
                  'descripcion': event.newData.descripcion,
                  'fechaBaja': event.newData.fechaBaja,
                  };
      this.service.updateMatarife(this.matarife).subscribe(datas => {
        event.confirm.resolve();
        console.log('se edito el matarife', datas);
    });
  }

}
