import { Component } from '@angular/core';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { Corralero } from './corralero';

@Component({
  selector: 'ngx-corraleros',
  templateUrl: './corraleros.component.html',
  styleUrls: ['./corraleros.component.scss'],
})
export class CorralerosComponent {

  // Add tipo-animal
  public corralero: Corralero = new Corralero();

  // Cargar paises en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      usuario: {
        title: 'Nombre',
        type: 'string',
        valuePrepareFunction: (data) => {
                                   return data['nombre'];
                               },
      },
      apellido: {
        title: 'Apellido',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.usuario.apellido;
          },
      },
      username: {
        title: 'Usename',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.usuario.username;
          },
      },
      password: {
        title: 'Contraseña',
        type: 'string',
        // TENGO QUE VER COMO MOSTRAR
        // valuePrepareFunction: (cell, row) => {
        //     return row.usuario.password;
        //   },
      },
      email: {
        title: 'E-mail',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.usuario.mail;
          },
      },
    },
  };

  constructor(public service: SmartTableService,
              ) {

                  // Obtengo los corraleros
                this.service.getCorraleros().subscribe(
                  data => {

                      this.source = data;
                      console.log('Se hizo el traspaso de corraleros');
                    },
                  );

  }
  // Agregar corralero
  onCreateConfirm(event) {
    this.corralero = {'id': event.newData.id,
                  'usuario': event.newData.persona,
                  };
      this.service.createCorralero(this.corralero).subscribe(datas => {
        event.confirm.resolve();
        console.log('se guardo un corralero', datas);
      });
  }

  // Borrar pais
  onDeleteConfirm(event): void {
    if (window.confirm('Estás seguro que quieres borrar?')) {
      const idcorralero: number = event.newData.id;
      this.service.deleteCorralero(idcorralero).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el corralero', datas);
      });
    } else {
      event.confirm.reject();
    }
  }
  // Editar pais
  onSaveConfirm(event) {
    this.corralero = {'id': event.newData.id,
                  'usuario': event.newData.persona,
                  };
      this.service.updateCorralero(this.corralero).subscribe(datas => {
        event.confirm.resolve();
        console.log('se edito el corralero', datas);
    });
  }


}
