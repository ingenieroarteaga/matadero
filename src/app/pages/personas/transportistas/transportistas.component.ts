import { Component } from '@angular/core';
import { Global } from '../../../global';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { Transportista } from './transportista';
import { Persona } from '../persona';

@Component({
  selector: 'ngx-transportistas',
  templateUrl: './transportistas.component.html',
  styleUrls: ['./transportistas.component.scss'],
})
export class TransportistasComponent {

  // URl base para obetener el servicio
  path: string = Global.urlBase ;

  // Add tipo-animal
  public transportista: Transportista = new Transportista();

  // Cargar paises en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      persona: {
        title: 'Nombre',
        type: 'string',
        valuePrepareFunction: (data) => {
                                   return data['nombre'];
                               },
      },
      apellido: {
        title: 'Apellido',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.persona.apellido;
          },
      },
      cuit: {
        title: 'Cuit',
        type: 'number',
        valuePrepareFunction: (cell, row) => {
            return row.persona.cuit;
          },
      },
      telefono: {
        title: 'Telefono',
        type: 'number',
        valuePrepareFunction: (cell, row) => {
            return row.persona.telefono;
          },
      },
      email: {
        title: 'E-mail',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.persona.email;
          },
      },
    },
  };

  constructor(public service: SmartTableService,
              ) {

                  // Obtengo los transportistas
                this.service.getTransportistas().subscribe(
                  data => {

                      this.source = data;
                      console.log('Se hizo el traspaso de transportistas');
                    },
                  );

  }
  // Agregar transportista
  onCreateConfirm(event) {
    this.transportista = {'id': event.newData.id,
                  'persona': event.newData.persona,
                  };
      this.service.createTransportista(this.transportista).subscribe(datas => {
        event.confirm.resolve();
        console.log('se guardo un transportista', datas);
      });
  }

  // Borrar pais
  onDeleteConfirm(event): void {
    if (window.confirm('Estás seguro que quieres borrar?')) {
      const idtransportista: number = event.newData.id;
      this.service.deleteTransportista(idtransportista).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el transportista', datas);
      });
    } else {
      event.confirm.reject();
    }
  }
  // Editar pais
  onSaveConfirm(event) {
    this.transportista = {'id': event.newData.id,
                  'persona': event.newData.persona,
                  };
      this.service.updateTransportista(this.transportista).subscribe(datas => {
        event.confirm.resolve();
        console.log('se edito el transportista', datas);
    });
  }


}
