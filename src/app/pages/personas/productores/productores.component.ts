import { Component } from '@angular/core';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { Productor } from './productor';

@Component({
  selector: 'ngx-productores',
  templateUrl: './productores.component.html',
  styleUrls: ['./productores.component.scss'],
})
export class ProductoresComponent {

  // Add productores
  public productor: Productor = new Productor();

  // Cargar paises en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      persona: {
        title: 'Nombre',
        type: 'string',
        valuePrepareFunction: (data) => {
                                   return data['nombre'];
                               },
      },
      apellido: {
        title: 'Apellido',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.persona.apellido;
          },
      },
      cuit: {
        title: 'Cuit',
        type: 'number',
        valuePrepareFunction: (cell, row) => {
            return row.persona.cuit;
          },
      },
      telefono: {
        title: 'Telefono',
        type: 'number',
        valuePrepareFunction: (cell, row) => {
            return row.persona.telefono;
          },
      },
      email: {
        title: 'E-mail',
        type: 'string',
        valuePrepareFunction: (cell, row) => {
            return row.persona.email;
          },
      },
    },
  };

  constructor(public service: SmartTableService,
              ) {

                  // Obtengo los productores
                this.service.getProductores().subscribe(
                  data => {

                      this.source = data;
                      console.log('Se hizo el traspaso de productores');
                    },
                  );

  }
  // Agregar productor
  onCreateConfirm(event) {
    this.productor = {'id': event.newData.id,
                  'id_persona': event.newData.persona,
                  };
      this.service.createProductor(this.productor).subscribe(datas => {
        event.confirm.resolve();
        console.log('se guardo un productor', datas);
      });
  }

  // Borrar pais
  onDeleteConfirm(event): void {
    if (window.confirm('Estás seguro que quieres borrar?')) {
      const idProductor: number = event.newData.id;
      this.service.deleteProductor(idProductor).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el Productor', datas);
      });
    } else {
      event.confirm.reject();
    }
  }
  // Editar pais
  onSaveConfirm(event) {
    this.productor = {'id': event.newData.id,
                  'id_persona': event.newData.persona,
                  };
      this.service.updateProductor(this.productor).subscribe(datas => {
        event.confirm.resolve();
        console.log('se edito el productor', datas);
    });
  }


}
