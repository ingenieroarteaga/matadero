import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PersonasComponent } from './personas.component';
import { ProductoresComponent } from './productores/productores.component';
import { MatarifesComponent } from './matarifes/matarifes.component';
import { TransportistasComponent } from './transportistas/transportistas.component';
import { AdministrativosComponent } from './administrativos/administrativos.component';
import { OperariosComponent } from './operarios/operarios.component';
import { CorralerosComponent } from './corraleros/corraleros.component';

const routes: Routes = [{
  path: '',
  component: PersonasComponent,
  children: [ {
      path: 'productores',
      component: ProductoresComponent,
      }, {
      path: 'matarifes',
      component: MatarifesComponent,
      }, {
      path: 'transportistas',
      component: TransportistasComponent,
      }, {
      path: 'administrativos',
      component: AdministrativosComponent,
      }, {
      path: 'operarios',
      component: OperariosComponent,
      }, {
      path: 'corraleros',
      component: CorralerosComponent,
      },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PersonasRoutingModule { }
export const routedComponents = [
  PersonasComponent,
  ProductoresComponent,
  MatarifesComponent,
  TransportistasComponent,
  AdministrativosComponent,
  OperariosComponent,
  CorralerosComponent,
];
