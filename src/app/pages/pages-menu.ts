import { NbMenuItem } from '@nebular/theme';

export const MENU_ITEMS: NbMenuItem[] = [
  {
    title: 'Operaciones',
    icon: 'nb-compose',
    children: [
      {
        title: 'Ingresos',
        link: '/pages/operaciones/ingresos',
      },
      {
        title: 'Faenas',
        link: '/pages/forms/layouts',
      },
      {
        title: 'Movimientos de Camara',
        link: '/pages/forms/buttons',
      },
      // {
      //   title: 'Datepicker',
      //   link: '/pages/forms/datepicker',
      // },
    ],
  },
  { title: 'Personas',
    icon: 'nb-person',
    children: [
      {
        title: 'Productores',
        link: '/pages/personas/productores',
      },
      {
        title: 'Matarifes',
        link: '/pages/personas/matarifes',
      },
      {
        title: 'Transportistas',
        link: '/pages/personas/transportistas',
      },
      {
        title: 'Administrativos',
        link: '/pages/personas/administrativos',
      },
      {
        title: 'Opeararios',
        link: '/pages/personas/operarios',
      },
      {
        title: 'Corraleros',
        link: '/pages/personas/corraleros',
      },
    ],
  },

  { title: 'Herramientas',
    icon: 'nb-gear',
    children: [
      {
        title: 'Animales',
        link: '/pages/herramientas/animales',
      },
      {
        title: 'Corrales',
        link: '/pages/herramientas/corrales',
      },
      {
        title: 'Camaras',
        link: '/pages/herramientas/camaras',
      },
      {
        title: 'Faenas',
        link: '/pages/herramientas/faenas',
      },
    ],
  },

  { title: 'Clasificadores',
    icon: 'nb-keypad',
    children: [
      {
        title: 'Paises',
        link: '/pages/clasificadores/paises',
      },
      {
        title: 'Provincias',
        link: '/pages/clasificadores/provincias',
      },
      {
        title: 'Localidades',
        link: '/pages/clasificadores/localidades',
      },
      {
        title: 'Distritos',
        link: '/pages/clasificadores/distritos',
      },
      {
        title: 'Tipos Animales',
        link: '/pages/clasificadores/tipos-animales',
      },
      {
        title: 'Subtipos Animales',
        link: '/pages/clasificadores/subtipos-animales',
      },
      {
        title: 'Tipos Camaras',
        link: '/pages/clasificadores/tipos-camaras',
      },
      {
        title: 'Tipos de Domicilio',
        link: '/pages/clasificadores/tipos-domicilio',
      },
      // {
      //   title: 'Tipos de Contactos',
      //   link: '/pages/clasificadores/tipos-contactos',
      // },
      {
        title: 'Tipos de Documento',
        link: '/pages/clasificadores/tipos-documento',
      },
      // {
      //   title: 'Login',
      //   link: '/pages/clasificadores/login',
      // },
    ],
  },
  // {
  //   title: 'E-commerce',
  //   icon: 'nb-e-commerce',
  //   link: '/pages/dashboard',
  //   home: true,
  // },
  // {
  //   title: 'IoT Dashboard',
  //   icon: 'nb-home',
  //   link: '/pages/iot-dashboard',
  // },
  // {
  //   title: 'FEATURES',
  //   group: true,
  // },
  // {
  //   title: 'Extra Components',
  //   icon: 'nb-star',
  //   children: [
  //     {
  //       title: 'Calendar',
  //       link: '/pages/extra-components/calendar',
  //     },
  //     {
  //       title: 'Stepper',
  //       link: '/pages/extra-components/stepper',
  //     },
  //     {
  //       title: 'List',
  //       link: '/pages/extra-components/list',
  //     },
  //     {
  //       title: 'Infinite List',
  //       link: '/pages/extra-components/infinite-list',
  //     },
  //     {
  //       title: 'Accordion',
  //       link: '/pages/extra-components/accordion',
  //     },
  //     {
  //       title: 'Progress Bar',
  //       link: '/pages/extra-components/progress-bar',
  //     },
  //     {
  //       title: 'Spinner',
  //       link: '/pages/extra-components/spinner',
  //     },
  //     {
  //       title: 'Alert',
  //       link: '/pages/extra-components/alert',
  //     },
  //     {
  //       title: 'Tree',
  //       link: '/pages/extra-components/tree',
  //     },
  //     {
  //       title: 'Tabs',
  //       link: '/pages/extra-components/tabs',
  //     },
  //     {
  //       title: 'Calendar Kit',
  //       link: '/pages/extra-components/calendar-kit',
  //     },
  //     {
  //       title: 'Chat',
  //       link: '/pages/extra-components/chat',
  //     },
  //   ],
  // },
  // {
  //   title: 'UI Features',
  //   icon: 'nb-keypad',
  //   link: '/pages/ui-features',
  //   children: [
  //     {
  //       title: 'Grid',
  //       link: '/pages/ui-features/grid',
  //     },
  //     {
  //       title: 'Icons',
  //       link: '/pages/ui-features/icons',
  //     },
  //     {
  //       title: 'Typography',
  //       link: '/pages/ui-features/typography',
  //     },
  //     {
  //       title: 'Animated Searches',
  //       link: '/pages/ui-features/search-fields',
  //     },
  //   ],
  // },
  // {
  //   title: 'Modal & Overlays',
  //   icon: 'nb-layout-default',
  //   children: [
  //     {
  //       title: 'Dialog',
  //       link: '/pages/modal-overlays/dialog',
  //     },
  //     {
  //       title: 'Window',
  //       link: '/pages/modal-overlays/window',
  //     },
  //     {
  //       title: 'Popover',
  //       link: '/pages/modal-overlays/popover',
  //     },
  //     {
  //       title: 'Toastr',
  //       link: '/pages/modal-overlays/toastr',
  //     },
  //     {
  //       title: 'Tooltip',
  //       link: '/pages/modal-overlays/tooltip',
  //     },
  //   ],
  // },
  // {
  //   title: 'Bootstrap',
  //   icon: 'nb-gear',
  //   children: [
  //     {
  //       title: 'Form Inputs',
  //       link: '/pages/bootstrap/inputs',
  //     },
  //     {
  //       title: 'Buttons',
  //       link: '/pages/bootstrap/buttons',
  //     },
  //     {
  //       title: 'Modal',
  //       link: '/pages/bootstrap/modal',
  //     },
  //   ],
  // },
  // {
  //   title: 'Maps',
  //   icon: 'nb-location',
  //   children: [
  //     {
  //       title: 'Google Maps',
  //       link: '/pages/maps/gmaps',
  //     },
  //     {
  //       title: 'Leaflet Maps',
  //       link: '/pages/maps/leaflet',
  //     },
  //     {
  //       title: 'Bubble Maps',
  //       link: '/pages/maps/bubble',
  //     },
  //     {
  //       title: 'Search Maps',
  //       link: '/pages/maps/searchmap',
  //     },
  //   ],
  // },
  // {
  //   title: 'Editors',
  //   icon: 'nb-title',
  //   children: [
  //     {
  //       title: 'TinyMCE',
  //       link: '/pages/editors/tinymce',
  //     },
  //     {
  //       title: 'CKEditor',
  //       link: '/pages/editors/ckeditor',
  //     },
  //   ],
  // },
  {
    title: 'Informes',
    icon: 'nb-tables',
    children: [
      {
        title: 'Ingresos',
        link: '/pages/tables/smart-table',
      },
    ],
  },
  // {
  //   title: 'Miscellaneous',
  //   icon: 'nb-shuffle',
  //   children: [
  //     {
  //       title: '404',
  //       link: '/pages/miscellaneous/404',
  //     },
  //   ],
  // },
  // {
  //   title: 'Auth',
  //   icon: 'nb-locked',
  //   children: [
  //     {
  //       title: 'Login',
  //       link: '/auth/login',
  //     },
  //     {
  //       title: 'Register',
  //       link: '/auth/register',
  //     },
  //     {
  //       title: 'Request Password',
  //       link: '/auth/request-password',
  //     },
  //     {
  //       title: 'Reset Password',
  //       link: '/auth/reset-password',
  //     },
  //   ],
  // },
];
