import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { OperacionesComponent } from './operaciones.component';

import { IngresosComponent } from './ingresos/ingresos.component';

const routes: Routes = [{
  path: '',
  component: OperacionesComponent,
  children: [ {
      path: 'ingresos',
      component: IngresosComponent,
      },
    ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class OperacionesRoutingModule { }
export const routedComponents = [
    OperacionesComponent,
    IngresosComponent,
  // TipoDocumentoComponent,
];
