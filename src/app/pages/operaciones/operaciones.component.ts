import { Component } from '@angular/core';

@Component({
  selector: 'ngx-operaciones',
  template: `
    <router-outlet></router-outlet>
  `,
})
export class OperacionesComponent {
}
