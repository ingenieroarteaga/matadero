import { NgModule } from '@angular/core';
import { Ng2SmartTableModule } from 'ng2-smart-table';

import { ThemeModule } from '../../@theme/theme.module';
import { OperacionesRoutingModule, routedComponents } from './operaciones-routing.module';
import { SmartTableService } from '../../@core/data/smart-table.service';
import { AutocompleteModule } from 'ng2-input-autocomplete';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';

@NgModule({
  imports: [
    ThemeModule,
    OperacionesRoutingModule,
    Ng2SmartTableModule,
    AutocompleteModule.forRoot(),
    Ng2AutoCompleteModule,

  ],
  declarations: [
    ...routedComponents,
  ],
  providers: [
    SmartTableService,
  ],
})
export class OperacionesModule { }
