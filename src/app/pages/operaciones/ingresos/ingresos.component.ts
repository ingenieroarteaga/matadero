// tslint:disable-next-line:max-line-length
import { Component } from '@angular/core';
import { Global } from '../../../global';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { AutocompleteModule } from 'ng2-input-autocomplete';
import { Ng2AutoCompleteModule } from 'ng2-auto-complete';

// import {Provincia} from '../provincias/provincia';
// import {Pais} from '../paises/pais';

@Component({
  selector: 'ngx-ingresos',
  templateUrl: './ingresos.component.html',
  styleUrls: ['./ingresos.component.scss'],
})
export class IngresosComponent {

  // variables para eventos en autocomplete
  selectedItem: any = '';
  inputChanged: any = '';

  matarifes: any = [];
  productores: any = [];
  transportistas: any = [];
  corraleros: any = [];
  corrales: any = [];
  tiposAnimal: any = [];
  subTiposAnimales: any = [];
  numeroTropa: any = [];

  configMatarife: any = {'class': 'auto-style', 'placeholder': 'Matarife', 'sourceField': ['persona', 'nombre']};
  configProductores: any = {'class': 'auto-style', 'placeholder': 'Productor', 'sourceField': ['persona', 'nombre']};
  configTranportistas: any = {'class': 'auto-style', 'placeholder': 'Tranportista',
                              'sourceField': ['persona', 'nombre']};
  configCorraleros: any = {'class': 'auto-style', 'placeholder': 'Corralero', 'sourceField': ['usuario', 'nombre']};
  configCorral: any = {'class': 'auto-style', 'placeholder': 'Corral', 'sourceField': ['persona', 'nombre']};
  configNumeroTropa: any = {'class': 'auto-style',  'placeholder': 'Número de Tropa',
                            'sourceField': ['persona', 'nombre']};



  constructor(service: SmartTableService) {
        // Obtengo los matarifes
        service.getMatarifes().subscribe(
          data => {

              this.matarifes = data;
              console.log(this.matarifes);
            },
        );
        // Obtengo los productores
        service.getProductores().subscribe(
          data => {

              this.productores = data;
              console.log(this.productores);
            },
        );
        // Obtengo los transportistas
        service.getTransportistas().subscribe(
          data => {

              this.transportistas = data;
              console.log(this.transportistas);
            },
        );
        // Obtengo los corraleros
        service.getCorraleros().subscribe(
          data => {

              this.corraleros = data;
              console.log(this.corraleros);
            },
        );
        // Obtengo los corrales
        // service.getCorrales().subscribe(
        //   data => {
        //
        //       this.corrales = data;
        //       console.log(this.corrales);
        //     },
        // );

        // Obtengo los tipo de animales
        service.getTiposAnimales().subscribe(
          data => {
              this.tiposAnimal = data;
              console.log(this.tiposAnimal);
            },
        );

        // Obtengo los subtippo de animales
        service.getSubTiposAnimales().subscribe(
          data => {
              this.subTiposAnimales = data;
              console.log(this.subTiposAnimales);
            },
        );

        // Obtengo los números de tropa del cliente
        // service.getNumeroDeTropa().subscribe(
        //   data => {
        //       this.numeroTropa = data;
        //       console.log(this.numeroTropa);
        //     },
        // );
  }
  onSelect(item: any) {
    this.selectedItem = item;
  }

  onInputChangedEvent(val: string) {
    this.inputChanged = val;
  }

}
