import { Localidad } from '../localidades/localidad';
// import { Domicilio } from "../../model/domicilio";

export class Distrito {
    public id: number;
    public descripcion: string;
    public codigoPostal: string;
    public fechaBaja: Date;
    public localidad: Localidad;
    public domicilioList: any[];
}
