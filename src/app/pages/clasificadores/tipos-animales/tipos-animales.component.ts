import { Component } from '@angular/core';
import { Global } from '../../../global';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { TipoAnimal } from './tipo-animal' ;

@Component({
  selector: 'ngx-tipos-animales',
  templateUrl: './tipos-animales.component.html',
  styleUrls: ['./tipos-animales.component.scss'],
})
export class TiposAnimalesComponent {

  // URl base para obetener el servicio
  path: string = Global.urlBase ;

  // Add tipo-animal
  public tipoAnimal: TipoAnimal = new TipoAnimal();

  // Cargar paises en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      descripcion: {
        title: 'Descripción',
        type: 'string',
      },
    },
  };

  constructor(public service: SmartTableService,
              ) {

                  // Obtengo los tipo de animales
                this.service.getTiposAnimales().subscribe(
                  data => {

                      this.source = data;
                      console.log('Se hizo el traspaso de tipo de animales');
                    },
                  );

  }
  // Agregar pais
  onCreateConfirm(event) {
    this.tipoAnimal = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  };
      this.service.createTipoAnimal(this.tipoAnimal).subscribe(datas => {
        event.confirm.resolve();
        console.log('se guardo un tipo de animal', datas);
      });
  }

  // Borrar pais
  onDeleteConfirm(event): void {
    if (window.confirm('Estás seguro que quieres borrar?')) {
      const idTipoAnimal: number = event.newData.id;
      this.service.deleteTipoAnimal(idTipoAnimal).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el tipo de animal', datas);
      });
    } else {
      event.confirm.reject();
    }
  }
  // Editar pais
  onSaveConfirm(event) {
    this.tipoAnimal = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  };
      this.service.updateTipoAnimal(this.tipoAnimal).subscribe(datas => {
        event.confirm.resolve();
        console.log('se edito el tipo de animal', datas);
    });
  }


}
