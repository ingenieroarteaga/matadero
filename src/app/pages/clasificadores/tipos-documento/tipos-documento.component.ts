import { Component } from '@angular/core';
import { Global } from '../../../global';
import { SmartTableService } from '../../../@core/data/smart-table.service';

import {TipoDocumento} from './tipo-documento';

@Component({
  selector: 'ngx-tipos-documento',
  templateUrl: './tipos-documento.component.html',
  styleUrls: ['./tipos-documento.component.scss'],
})
export class TiposDocumentoComponent {

  // URl base para obetener el servicio
  path: string = Global.urlBase ;

  // Trabajar con tipo-domicilio
  public tipoDocumento: TipoDocumento = new TipoDocumento();

  // Cargar tipo-domicilios en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      descripcion: {
        title: 'Tipo de Documento',
        type: 'string',
      },
    },
  };

  constructor(public service: SmartTableService,
              ) {
                // Obtengo los tipo de documentos
                this.service.getTipoDocumentos().subscribe(
                  data => {

                      this.source = data;
                      console.log(this.source);
                    },
                  );
  }

  // Agregar provincia
  onCreateConfirm(event) {
    this.tipoDocumento = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'idFe': event.newData.idFe,
                  'fechaBaja': event.newData.fechaBaja,
                  'personaList': event.newData.personaList,
                  };
      this.service.createTipoDocumento(this.tipoDocumento).subscribe(datas => {
        event.confirm.resolve();
        console.log('se guardo un tipo de documento', datas);
      });
  }

  // Borrar provincia
  onDeleteConfirm(event): void {
    if (window.confirm('Estás seguro que quieres borrar?')) {
      const idTipoDocumento: number = event.newData.id;
      this.service.deleteTipoDomicilio(idTipoDocumento).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el tipo de documento', datas);
      });
    } else {
      event.confirm.reject();
    }
  }
  // Editar provincia
  onSaveConfirm(event) {
    this.tipoDocumento = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'idFe': event.newData.idFe,
                  'fechaBaja': event.newData.fechaBaja,
                  'personaList': event.newData.personaList,
                  };
      this.service.updateTipoDocumento(this.tipoDocumento).subscribe(datas => {
        event.confirm.resolve();
        console.log('se edito el tipo de documento', datas);
    });
  }


}
