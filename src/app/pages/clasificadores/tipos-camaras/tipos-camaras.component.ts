import { Component } from '@angular/core';
import { Global } from '../../../global';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { TipoCamara } from './tipo-camara';

@Component({
  selector: 'ngx-tipos-camaras',
  templateUrl: './tipos-camaras.component.html',
  styleUrls: ['./tipos-camaras.component.scss'],
})
export class TiposCamarasComponent {

  // URl base para obetener el servicio
  path: string = Global.urlBase ;

  // Add tipo-animal
  public tipoCamara: TipoCamara = new TipoCamara();

  // Cargar paises en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      descripcion: {
        title: 'Descripción',
        type: 'string',
      },
      tempDesde: {
        title: 'Temperatura Desde',
        type: 'string',
      },
      tempHasta: {
        title: 'Temperatura Hasta',
        type: 'string',
      },
      precio: {
        title: 'Precio',
        type: 'string',
      },
    },
  };

  constructor(public service: SmartTableService,
              ) {

                  // Obtengo los tipo de animales
                this.service.getTiposCamaras().subscribe(
                  data => {

                      this.source = data;
                      console.log('Se hizo el traspaso de tipo de Camaras');
                    },
                  );

  }
  // Agregar pais
  onCreateConfirm(event) {
    this.tipoCamara = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'tempDesde': event.newData.descripcion,
                  'tempHasta': event.newData.descripcion,
                  'precio': event.newData.descripcion,
                  'fechaBaja': event.newData.descripcion,
                  };
      this.service.createTipoCamara(this.tipoCamara).subscribe(datas => {
        event.confirm.resolve();
        console.log('se guardo un tipo de Camara', datas);
      });
  }

  // Borrar pais
  onDeleteConfirm(event): void {
    if (window.confirm('Estás seguro que quieres borrar?')) {
      const idTipoCamara: number = event.newData.id;
      this.service.deleteTipoCamara(idTipoCamara).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el tipo de Camara', datas);
      });
    } else {
      event.confirm.reject();
    }
  }
  // Editar pais
  onSaveConfirm(event) {
    this.tipoCamara = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'tempDesde': event.newData.descripcion,
                  'tempHasta': event.newData.descripcion,
                  'precio': event.newData.descripcion,
                  'fechaBaja': event.newData.descripcion,
                  };
      this.service.updateTipoCamara(this.tipoCamara).subscribe(datas => {
        event.confirm.resolve();
        console.log('se edito el tipo de Camara', datas);
    });
  }


}
