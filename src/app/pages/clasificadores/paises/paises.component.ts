import { Component } from '@angular/core';
import { Global } from '../../../global';
import { SmartTableService } from '../../../@core/data/smart-table.service';

import {Pais} from './pais';
import {Provincia} from '../provincias/provincia';

@Component({
  selector: 'ngx-paises',
  templateUrl: './paises.component.html',
  styleUrls: ['./paises.component.scss'],
})

export class PaisesComponent {

  // URl base para obetener el servicio
  path: string = Global.urlBase ;

  // Add pais
  public pais: Pais = new Pais();

  // Cargar paises en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      descripcion: {
        title: 'Descripción',
        type: 'string',
      },
      fechaBaja: {
        title: 'Fecha Baja',
        type: 'string',
        editable: false,
        addable: false,
      },
    },
  };

  constructor(public service: SmartTableService,
              ) {

                  // Obtengo los paises
                this.service.getPaises().subscribe(
                  data => {

                      this.source = data;
                      console.log('Se hizo el traspaso de paises');
                    },
                  );

  }
  // Agregar pais
  onCreateConfirm(event) {
    this.pais = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'fechaBaja' : event.newData.fechaBaja,
                  // 'provinciaList': event.newData.provinciaList,
                  };
      this.service.createPais(this.pais).subscribe(datas => {
        event.confirm.resolve();
        console.log('se guardo un pais', datas);
      });
  }

  // Borrar pais
  onDeleteConfirm(event): void {
    if (window.confirm('Estás seguro que quieres borrar?')) {
      const idPais: number = event.newData.id;
      this.service.deletePais(idPais).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro el pais', datas);
      });
    } else {
      event.confirm.reject();
    }
  }
  // Editar pais
  onSaveConfirm(event) {
    this.pais = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'fechaBaja' : event.newData.fechaBaja,
                  // 'provinciaList': event.newData.provinciaList,
                  };
      this.service.updatePais(this.pais).subscribe(datas => {
        event.confirm.resolve();
        console.log('se edito el pais', datas);
    });
  }
}
