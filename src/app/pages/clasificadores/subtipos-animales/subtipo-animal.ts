import { TipoAnimal } from '../tipos-animales/tipo-animal';

export class SubTipoAnimal {
        public id: number;
        public tipoAnimal: TipoAnimal;
        public descripcion: string;
}
