import { Component } from '@angular/core';
import { Global } from '../../../global';
import { SmartTableService } from '../../../@core/data/smart-table.service';

import {Provincia} from './provincia';
import {Pais} from '../paises/pais';

@Component({
  selector: 'ngx-provincias',
  templateUrl: './provincias.component.html',
  styleUrls: ['./provincias.component.scss'],
})
export class ProvinciasComponent  {

  // URl base para obetener el servicio
  path: string = Global.urlBase ;

  // Cargo los paises disponibles
  paisesCargador;
  paises: Pais[];
  paisCargador: Pais = new Pais();

  // Trabajar con provincia
  public provincia: Provincia = new Provincia();

  // Cargar provincias en html
  source: any = [];

  // Datos para crear la tabla
  settings = {
    add: {
      addButtonContent: '<i class="nb-plus"></i>',
      createButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmCreate: true,
    },
    edit: {
      editButtonContent: '<i class="nb-edit"></i>',
      saveButtonContent: '<i class="nb-checkmark"></i>',
      cancelButtonContent: '<i class="nb-close"></i>',
      confirmSave: true,
    },
    delete: {
      deleteButtonContent: '<i class="nb-trash"></i>',
      confirmDelete: true,
    },
    columns: {
      id: {
        title: 'ID',
        type: 'number',
        editable: false,
        addable: false,
      },
      descripcion: {
        title: 'Descripción',
        type: 'string',
      },
      fechaBaja: {
        title: 'Fecha Baja',
        type: 'string',
        editable: false,
        addable: false,
      },
      pais: {
        title: 'Pais',
        valuePrepareFunction: (data) => {
                                   return data['descripcion'];
                               },
        editor: {
          type: 'list',
          config: {
            list: this.paises,
          },
        },
      },
    },
  };

  constructor(public service: SmartTableService,
              ) {

                // Obtengo los paises que muestro en editar
                this.service.getPaises().subscribe(
                  data => {
                    this.paises = data;
                    this.paisesCargador = data.map( item => {
                    return { title: item.descripcion , value : item.id };
                    });
                    // asigno a la variable setting los paises que debe mostrar en editor
                    this.settings.columns.pais.editor.config.list = this.paisesCargador;
                    this.settings = Object.assign({}, this.settings);
                    console.log(this.paisesCargador);
                    console.log(this.paises);
                    },
                  );

                // Obtengo las provincias
                this.service.getProvincias().subscribe(
                  data => {

                      this.source = data;
                      console.log(this.source);
                    },
                  );

  }

  // Agregar provincia
  onCreateConfirm(event) {
    this.provincia = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'fechaBaja' : event.newData.fechaBaja,
                  'pais': this.provincia.pais = new Pais(),
                  'localidadList': event.newData.localidadList,
                  };
      this.provincia.pais =  this.paises.find(pais => pais.id == event.newData.pais);
      console.log(this.provincia);
      this.service.createProvincia(this.provincia).subscribe(datas => {
        event.confirm.resolve();
        console.log('se guardo una provincia', datas);
      });
  }

  // Borrar provincia
  onDeleteConfirm(event): void {
    if (window.confirm('Estás seguro que quieres borrar?')) {
      const idProvincia: number = event.newData.id;
      this.service.deleteProvincia(idProvincia).subscribe(datas => {
        event.confirm.resolve();
        console.log('se borro la provincia', datas);
      });
    } else {
      event.confirm.reject();
    }
  }
  // Editar provincia
  onSaveConfirm(event) {
    this.provincia = {'id': event.newData.id,
                  'descripcion': event.newData.descripcion,
                  'fechaBaja' : event.newData.fechaBaja,
                  'pais': this.provincia.pais = new Pais(),
                  'localidadList': event.newData.localidadList,
                  };
      this.provincia.pais =  this.paises.find(pais => pais.id == event.newData.pais);
      console.log(this.provincia);
      this.service.updateProvincia(this.provincia).subscribe(datas => {
        event.confirm.resolve();
        console.log('se edito la provincia', datas);
    });
  }


}
