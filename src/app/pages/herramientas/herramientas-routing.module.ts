import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HerramientasComponent } from './herramientas.component';
import { AnimalesComponent } from './animales/animales.component';
import { CorralesComponent } from './corrales/corrales.component';
import { CamarasComponent } from './camaras/camaras.component';
import { FaenasComponent } from './faenas/faenas.component';

const routes: Routes = [{
  path: '',
  component: HerramientasComponent,
  children: [ {
      path: 'animales',
      component: AnimalesComponent,
      }, {
      path: 'corrales',
      component: CorralesComponent,
      }, {
      path: 'camaras',
      component: CamarasComponent,
      }, {
      path: 'faenas',
      component: FaenasComponent,
      },
  ],
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HerramientasRoutingModule { }
export const routedComponents = [
  HerramientasComponent,
  AnimalesComponent,
  CorralesComponent,
  CamarasComponent,
  FaenasComponent,
];
