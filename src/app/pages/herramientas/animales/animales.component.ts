import { Component } from '@angular/core';
import { Global } from '../../../global';
import { SmartTableService } from '../../../@core/data/smart-table.service';
import { Animal } from './animal' ;

@Component({
  selector: 'ngx-animales',
  templateUrl: './animales.component.html',
  styleUrls: ['./animales.component.scss'],
})
export class AnimalesComponent {

  constructor(public animal: Animal) { }


}
